import { Component } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';

import { HttpClient } from '@angular/common/http';
import { SmartlistPersonnageComponent } from './smartlist-personnage/smartlist-personnage.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'fend01';
  
  constructor(private http: HttpClient) { }



}