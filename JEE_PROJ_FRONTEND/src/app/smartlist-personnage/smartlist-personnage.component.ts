import { Component, OnInit } from '@angular/core';

import { ServerDataSource } from 'ng2-smart-table';

import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-smartlist-personnage',
  templateUrl: './smartlist-personnage.component.html',
  styleUrls: ['./smartlist-personnage.component.css']
})
export class SmartlistPersonnageComponent implements OnInit {
  dsource: ServerDataSource;

  
  settingss = {
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      nom: {
        title: 'Nom',
        editable: true
      },
      taille: {
        title: 'Title'
      },
      poids: {
        title: 'Poids'
      },
      type: {
        title: 'type'
      }
    }

  };


  constructor(private http: HttpClient) { 
    this.dsource = new ServerDataSource(http, { endPoint: 'http://localhost:8080/personnage/all' });
      
    http.get('http://localhost:8080/personnage/all').subscribe(data => {
      console.log(data);
    });

  }

  ngOnInit() {
    
  }

  onDeleteConfirms(event) {
    if (window.confirm('Are you sure you want to delete?')) {
      this.http.get('http://localhost:8080/personnage/delete?id='+event.newData['id']).subscribe(data => {
        console.log(data);
        event.confirm.resolve();

      });

    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirms(event) {


    if (window.confirm('Are you sure you want to save?'+event.data.nom)) {

      this.http.get('http://localhost:8080/personnage/update?id='+event.newData['id']+
      '&nom='+event.newData['nom']+
      '&taille='+event.newData['taille']+
      '&poids='+event.newData['poids']+'&arme='+event.newData.arme['nom']+'&type='+event.newData['type']).subscribe(data => {
        console.log(data);
      });


      event.confirm.resolve(event.newData);
    } else {
      event.confirm.reject();
    }
  }

  onCreateConfirms(event) {
    if (window.confirm('Are you sure you want to create?')) {
      event.newData['name'] += ' + added in code';
      event.confirm.resolve(event.newData);
    } else {
      event.confirm.reject();
    }
  }

    
  onUserSelectR(event) {
    window.confirm('L\'arme est de type : ' + event.data.arme.type + ' et de description : '+ event.data.arme.description);
  }



}
