import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartlistPersonnageComponent } from './smartlist-personnage.component';

describe('SmartlistPersonnageComponent', () => {
  let component: SmartlistPersonnageComponent;
  let fixture: ComponentFixture<SmartlistPersonnageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartlistPersonnageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartlistPersonnageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
