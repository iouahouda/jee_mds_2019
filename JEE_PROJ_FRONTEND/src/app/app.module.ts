import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewArmeComponent } from './new-arme/new-arme.component';
import { SmartlistPersonnageComponent } from './smartlist-personnage/smartlist-personnage.component';
import { SmartlistArmeComponent } from './smartlist-arme/smartlist-arme.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const appRoutes: Routes = [
  { path: '', component: AppComponent },
  { path: 'Personnages', component: SmartlistPersonnageComponent },
  { path: 'Armes',      component: SmartlistArmeComponent },
  { path: 'NouvArme',      component: NewArmeComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NewArmeComponent,
    SmartlistPersonnageComponent,
    SmartlistArmeComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    Ng2SmartTableModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
