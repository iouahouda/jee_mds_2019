import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewArmeComponent } from './new-arme.component';

describe('NewArmeComponent', () => {
  let component: NewArmeComponent;
  let fixture: ComponentFixture<NewArmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewArmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewArmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
