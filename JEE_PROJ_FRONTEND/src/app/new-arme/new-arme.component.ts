import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-new-arme',
  templateUrl: './new-arme.component.html',
  styleUrls: ['./new-arme.component.css']
})
export class NewArmeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
  }

  onSubmit(f: NgForm) {
    console.log(f.value);  // { first: '', last: '' }
    console.log(f.valid);  // false
  }


}
