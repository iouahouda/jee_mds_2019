import { Component, OnInit } from '@angular/core';
import { ServerDataSource } from 'ng2-smart-table';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-smartlist-arme',
  templateUrl: './smartlist-arme.component.html',
  styleUrls: ['./smartlist-arme.component.css']
})
export class SmartlistArmeComponent implements OnInit {

  dsource2: ServerDataSource;



  settingss2 = {
    columns: {
      nom: {
        title: 'Nom',
        editable: true
      },
      type: {
        title: 'type'
      },
      distance: {
        title: 'distance'
      },
      description: {
        title: 'description'
      },
      power: {
        title: 'power'
      }
    }

  };

  constructor(private http: HttpClient) {

    this.dsource2 = new ServerDataSource(http, { endPoint: 'http://localhost:8080/arme/all' });

   }

  ngOnInit() {
  }

}
