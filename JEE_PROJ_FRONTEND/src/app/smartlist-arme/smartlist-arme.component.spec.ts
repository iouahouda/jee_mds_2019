import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartlistArmeComponent } from './smartlist-arme.component';

describe('SmartlistArmeComponent', () => {
  let component: SmartlistArmeComponent;
  let fixture: ComponentFixture<SmartlistArmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartlistArmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartlistArmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
