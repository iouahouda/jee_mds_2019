package com.uvsq.dao;

import org.springframework.data.repository.CrudRepository;

import com.uvsq.entity.Personnage;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PersonnageRespository extends CrudRepository<Personnage, Long> {

}
