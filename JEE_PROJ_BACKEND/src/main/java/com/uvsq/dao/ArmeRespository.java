package com.uvsq.dao;

import org.springframework.data.repository.CrudRepository;

import com.uvsq.entity.Arme;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ArmeRespository extends CrudRepository<Arme, Long> {

}
