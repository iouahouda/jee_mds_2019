package com.uvsq.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;
@Entity
@Data
public class Arme{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String nom;
	private String type;
	private int distance;
	private String description;
	private int power = 50;
	
	public Arme() {
		
	}
	
	@Override
	public String toString() {
		return "Arc [nom=" + nom + ", type=" + type + ", distance=" + distance + ", description=" + description
				+ ", power=" + power + "]";
	}

	public Arme(String nom, String type, int distance, String description, int power) {
		this.nom = nom;
		this.type = type;
		this.distance = distance;
		this.description = description;
		this.power = power;
	}
}
