package com.uvsq.utils;

import org.springframework.stereotype.Service;

import com.uvsq.entity.Arme;

@Service
public class ArmeFactory {
	
	public Arme getArme(String str) {
		if(str.equals("Epee")) {
			return new Arme("Epee","Epee",2,"Epee normal",40);
		}
		else if(str.equals("Narsil-Anduril")) {
			return new Arme("Narsil-Anduril","Epee",4,"Epee de Narsil-Anduril",85);
		}
		else if(str.equals("Orcrist")) {
			return new Arme("Orcrist","Epee",3,"Epee de Orcrist",70);
		}		
		else if(str.equals("Flèche noire")) {
			return new Arme("Flèche noire","Flèche",3,"Flèche normal",70);
		}		
		else 
			return null;
	}
}
