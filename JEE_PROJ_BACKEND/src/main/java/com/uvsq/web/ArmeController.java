package com.uvsq.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uvsq.dao.ArmeRespository;
import com.uvsq.entity.Arme;
import com.uvsq.entity.Personnage;

@Controller
@RequestMapping(path="/arme") 
@CrossOrigin(origins = "http://localhost:4200")
public class ArmeController {
	Logger logger = LoggerFactory.getLogger(PersonnageController.class);
	
	@Autowired
	private ArmeRespository pArme;
	
	@GetMapping(path="/add")
	public @ResponseBody Arme add(

			@RequestParam String  nom,
			@RequestParam String  type,
			@RequestParam int  distance,
			@RequestParam String  description,
			@RequestParam int  power
			)
	{
		
		Arme a = new Arme(nom,type,distance,description,power);
		Arme res = pArme.save(a);
		
		return res;
	}
	
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<Arme> all() {
		logger.info("Arme :: >> GET REQUEST FOR ALL");
		return pArme.findAll();
	}
	
	
}
