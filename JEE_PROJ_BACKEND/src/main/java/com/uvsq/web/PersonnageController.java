package com.uvsq.web;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uvsq.dao.PersonnageRespository;
import com.uvsq.entity.Personnage;
import com.uvsq.utils.ArmeFactory;

@Controller
@RequestMapping(path="/personnage") 
@CrossOrigin(origins = "http://localhost:4200")

public class PersonnageController {
	
	Logger logger = LoggerFactory.getLogger(PersonnageController.class);

	
	@Autowired
	private PersonnageRespository pRepo;
	
	
	@Autowired
	private ArmeFactory af;
	
	@GetMapping(path="/add")
	public @ResponseBody String addNewPersonnage(
			@RequestParam String  nom,
			@RequestParam double  taille,
			@RequestParam double  poids,
			@RequestParam String  arme,
			@RequestParam String  type		
			)
	{
		Personnage p = new Personnage();
		p.setNom(nom);
		p.setPoids(poids);
		p.setTaille(taille);
		p.setType(type);
		p.setArme(af.getArme(arme));
		pRepo.save(p);
		return "Saved";
	}
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<Personnage> getAllUsers() {
		logger.info("GET REQUEST FOR ALL");
		return pRepo.findAll();
	}
	
	
	@GetMapping("/update")
	public Personnage updatePersonnage(
			@RequestParam long id,
			@RequestParam String  nom,
			@RequestParam double  taille,
			@RequestParam double  poids,
			@RequestParam String  arme,
			@RequestParam String  type		
			){

		Optional<Personnage> personnageOptional = pRepo.findById(id);

		if (!personnageOptional.isPresent())
			return null;
		
		logger.info("GET UPDATE REQUEST for " + personnageOptional.get().toString());
 
		Personnage p = new Personnage();
		p.setNom(nom);
		p.setId(id);
		p.setPoids(poids);
		p.setTaille(taille);
		p.setType(type);
		p.setArme(af.getArme(arme));
		
		
		pRepo.save(p);

		return personnageOptional.get();
	}
	
	@GetMapping("/delete")
	public ResponseEntity<Personnage> removePersonnage(
			@RequestParam long id
			){
		Optional<Personnage> personnageOptional = pRepo.findById(id);

		if (!personnageOptional.isPresent())
			return ResponseEntity.notFound().build();
		
		pRepo.delete(personnageOptional.get());
		
		return ResponseEntity.noContent().build();
	}
}
